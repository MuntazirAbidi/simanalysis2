module HaloTYPE

  type Halo

     integer  :: HN                       ! number of particles in the halo                  
     real(8) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     real(8), dimension(3) :: HPos       ! centre of mass position [Mpc/h]
     real(8), dimension(3) :: HVel       ! centre of mass velocity [km/s]

     real(8) :: Hrad                     ! radius of halo = mean of last 4 part.
     real(8) :: Hradhalf                 ! Half-mass radius
     real(8) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     real(8), dimension(6) :: HInert     ! Inertia matrix: 
                                          ! I11, I22, I33, I12, I13, I23
  end type Halo

end module HaloTYPE

program power
USE halotype
USE eval           ! Frequently used Routines for Correlation and Power Spectrum
USE snapshot       ! Reads in Dark Matter Particles 
implicit none


! All that in and out Files
character*200 :: Folder,Folder2
character*200 :: InFileBase1,InFileBase2
character*200 :: PsFileBase1, PsOutFile1,Extension1,PsFileBase2, PsOutFile2,Extension2
character*200 :: datadir,CrossFileBase,HalInFile,HaloFileBase,HaloOutFile
character*10 :: nodestr,snapstr,ncstr, snapstr2

integer :: idat                ! =1 read data; =0 get header
integer :: iPos                ! =1 read positions; =0 do not.
integer :: iVel                ! =1 read velocities; =0 do not.
integer :: iID                 ! =1 read the ID's; =0 do not.
integer :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-) 
type(snapFILES) :: sF
	
! Switches	    
integer :: doCorrect						! do CIC correction or not
integer :: doBinnedH
integer :: NodeNumber
integer :: FileNumber, FileNumber2
integer :: NHalBin,iMBin,cnt,NHalTot


type(halo) :: HaloFinal
integer, parameter :: NMassBins=5
real, dimension(NMassBins+1) :: MBinB
integer, dimension(NMassBins+1) :: NBinB
real, dimension(NMassBins) :: MBinC
integer, dimension(NMassBins) :: NHalosBin
real(4), dimension(:), allocatable :: HalM
real(4), dimension(:,:), allocatable :: HalPos,HalVel
	
	
real(4) :: radius

! Box dimensions
real(8), dimension(:,:,:), allocatable :: deltadm,deltag, deltadmsquare, deltadml,tidal
real(8), dimension(:,:,:), allocatable :: deltar, deltaprod

integer, dimension(NkBins) :: kBinCnt
real(8), dimension(NkBins) :: kTrueBinC
real(8), dimension(NkBins) :: powerdfdf   ! matter-matter power spectra
real(8), dimension(NKbins) :: powerdmldmn ! cross spectra <dm_linear, dm_nonlinear> linear and non-linear matter density (propagator)
real(8), dimension(NkBins,NMassBins) :: powerdmdg,powerdgdg,powerddmldg	! halo-halo and halo-matter power spectra  

 
real(8), dimension(NKbins) :: powerddmldm,powerddmlddml,powerddmlS2 ! cross spectra <sij,dm>, <dm^2,dm>, <shift,dm>

real :: time1,time2
integer :: OMP_GET_NUM_THREADS,TID,OMP_GET_THREAD_NUM

integer :: i

radius = 20.0


	
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
call cpu_time(time1)
! Size of FFT Grid	
NCell=512
box=1500.
! Redshift Output
FileNumber=9
! Realization
NodeNumber=1
FileNumber2=999
call genbink(0.003d0,0.5d0,'log')


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
!				> > > FILENAMES < < <
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (FileNumber<10) then	
    write(snapstr,'(a2,i1)') '00',FileNumber
elseif (FileNumber > 9 .and. FileNumber <100) then
    write(snapstr,'(a1,i2)') '0',FileNumber
elseif (FileNumber > 99 .and. FileNumber <1000) then
    write (snapstr, '(i3)') FileNumber
endif

if (FileNumber2<10) then
   write(snapstr2, '(a2,i1)') '00', FileNumber2 
elseif (FileNumber2 > 9 .and. FileNumber2 < 100) then 
   write(snapstr2,'(a1,i2)') '0', FileNumber2
elseif (FileNumber2 >  99 .AND. FileNumber2 <  1000) then 
   write(snapstr2, '(i3)')  FileNumber2
endif

write (*,*) 'snapstr1 = ', snapstr
write(*,*) 'snapstr2 = ', snapstr2

!simulation
!if (NodeNumber<10) then
!    write(nodestr,'(i1)') NodeNumber
!else
!    write(nodestr,'(i2)') NodeNumber
!endif

call getarg(1,nodestr)

if (Ncell<100) then
    write(ncstr,'(i2)') Ncell
elseif (Ncell<1000) then
    write(ncstr,'(i3)') Ncell
elseif (Ncell<10000) then
    write(ncstr,'(i4)') Ncell
endif

doCorrect=1
doBinnedH=1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Output File Names
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Extension1='NODE'//trim(nodestr)//'_'//assign//'C1_'//trim(ncstr)//'Smoothing_R20_'
Folder='/fast/space/projects/dp002/dc-abid1/projects/powerspec/Data/cross_spectra/quardatic/quadratic_quadratic/'		

PsFileBase1='deltaGsquare_deltaGsquare_'
PsOutFile1=trim(Folder)//trim(PsFileBase1)//trim(Extension1)//trim(snapstr2)//'_'//trim(snapstr)//'_log.dat'
write(*,*) 'Writing delta^2-delta^2 cross spectra to ',PsOutFile1


PsFileBase2='deltaGsquare_tidalG_'
PsOutFile2=trim(Folder)//trim(PsFileBase2)//trim(Extension1)//trim(snapstr2)//'_'//trim(snapstr)//'_log.dat'
write(*,*) 'Writing delta^2-deltah cross spectra to ',PsOutFile2


!HaloFileBase='deltaGsquare_deltahalo_'
!HaloOutFile=trim(Folder)//trim(HaloFileBase)//trim(Extension)//trim(snapstr)//'.dat'
!write(*,*) 'Writing Halo cross spectrum to ',HaloOutFile

!Halo Number Mass Bins
NbinB=(/20,60,180,540,1620,4860/)

!datadir='/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/reducedGROUPS/'
!HalInFile=trim(datadir)//'GRPS_CMS_wmap7_fid_run'//trim(nodestr)//'_'//trim(snapstr)
	

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
! First FFT - Determines FFT strategy
allocate(deltar(1:NCell+2,1:NCell,1:NCell))
call fft3d(deltar,deltar,'f')	
deallocate(deltar)


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////		

write(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
write(*,'(a)') '          > > > Loading linear dark matter < < <'
write(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'
		

allocate(deltadml(1:NCell+2,1:NCell,1:NCell))
allocate(deltadmsquare(1:NCell+2,1:NCell,1:NCell))
allocate(tidal(1:NCell+2,1:NCell,1:NCell))

doCorrect=1   
deltadml=0.
deltadmsquare=0

iDat=1; iPos=0; iVel=0; iID=0; iRed=0

! Fiducial Cosmology Final - CIC version of the Zeldovich initial conditions. This field has small scale artifacts
sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/ICs/'
sf%SNAPBASE = 'wmap7_fid_run'//trim(nodestr)//'_'
sf%SNAPEXT = snapstr2
sf%SNAPNFILES = 64


call read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadml)
call normalize(deltadml)                     ! dml in configuation space

call fft3d(deltadml,deltadml,'f')  ! dml in Fourier space 

if (doCorrect==1) then
    call ciccorrect(deltadml,1.0d0)
endif

call smdir(deltadml,radius,NCell) ! smoothing of the linear field

call S2(deltadml,deltadml,1.0d0,tidal)
call fft3d(tidal,tidal,'f')

call fft3d(deltadml,deltadml,'e')
deltadmsquare = deltadml*deltadml

call fft3d(deltadmsquare,deltadmsquare,'f')



deallocate (deltadml)


    allocate(deltar(1:NCell+2,1:NCell,1:NCell))
    deltar=0.
    call comprod(deltadmsquare,deltadmsquare,deltar)
    call powerspectrum(deltar,kBinCnt,kTrueBinC,powerddmlS2)	
    deallocate(deltar)

    allocate(deltar(1:NCell+2,1:NCell,1:NCell))
    deltar=0.
    call comprod(deltadmsquare,deltadmsquare,deltar)
    call powerspectrum(deltar,kBinCnt,kTrueBinC,powerddmlddml)	
    deallocate(deltar)

!Write Output
 open(20,file=PsOutFile1,form='formatted',status='replace')
    do i=1,NkBins
	write(20,'(2ES30.10,1I30,1ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerddmlddml(i)
    enddo
 close(20)

!Write Output
 open(21,file=PsOutFile2,form='formatted',status='replace')
    do i=1,NkBins
	write(21,'(2ES30.10,1I30,1ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerddmlS2(i)
    enddo
 close(21)


				
deallocate(tidal)
deallocate(deltadmsquare)

call cpu_time(time2)
time2=(time2-time1)
write(*,'(a,f8.2)') 'All done in', time2


end program power

