module HaloTYPE

  type Halo

     integer  :: HN                       ! number of particles in the halo                  
     real(8) :: Hmass                    ! Halo Mass fof=0.2 [10**10 Mo/h]

     real(8), dimension(3) :: HPos       ! centre of mass position [Mpc/h]
     real(8), dimension(3) :: HVel       ! centre of mass velocity [km/s]

     real(8) :: Hrad                     ! radius of halo = mean of last 4 part.
     real(8) :: Hradhalf                 ! Half-mass radius
     real(8) :: HDisp1D                  ! 1D velocity dispersion [km/s]

     real(8), dimension(6) :: HInert     ! Inertia matrix: 
                                          ! I11, I22, I33, I12, I13, I23
  end type Halo

end module HaloTYPE

program power
USE halotype
USE eval           ! Frequently used Routines for Correlation and Power Spectrum
USE snapshot       ! Reads in Dark Matter Particles 
implicit none


! All that in and out Files
character*200 :: Folder,Folder2
character*200 :: InFileBase,InFileBase2
character*200 :: PsFileBase, PsOutFile,Extension,PsFileBase2, PsOutFile2,Extension2
character*200 :: datadir,CrossFileBase,HalInFile,HaloFileBase,HaloOutFile
character*10 :: nodestr,snapstr,ncstr, snapstr2

integer :: idat                ! =1 read data; =0 get header
integer :: iPos                ! =1 read positions; =0 do not.
integer :: iVel                ! =1 read velocities; =0 do not.
integer :: iID                 ! =1 read the ID's; =0 do not.
integer :: iRed                ! =1 no redshift space distortions
                                 ! = (2,3,4) apply distortion in (x-,y-,z-) 
type(snapFILES) :: sF
	
! Switches	    
integer :: doCorrect						! do CIC correction or not
integer :: doBinnedH
integer :: NodeNumber
integer :: FileNumber, FileNumber2
integer :: NHalBin,iMBin,cnt,NHalTot


type(halo) :: HaloFinal
integer, parameter :: NMassBins=5
real, dimension(NMassBins+1) :: MBinB
integer, dimension(NMassBins+1) :: NBinB
real, dimension(NMassBins) :: MBinC
integer, dimension(NMassBins) :: NHalosBin
real(4), dimension(:), allocatable :: HalM
real(4), dimension(:,:), allocatable :: HalPos,HalVel
	
	
real(4) :: radius

! Box dimensions
real(8), dimension(:,:,:), allocatable :: deltadm,deltag, deltadmsquare,deltadmnlsquare, deltadml
real(8), dimension(:,:,:), allocatable :: deltar, deltaprod

integer, dimension(NkBins) :: kBinCnt
real(8), dimension(NkBins) :: kTrueBinC
real(8), dimension(NkBins) :: powerdfdf   ! matter-matter power spectra
real(8), dimension(NKbins) :: powerdmldmn ! cross spectra <dm_linear, dm_nonlinear> linear and non-linear matter density (propagator)
real(8), dimension(NkBins,NMassBins) :: powerdmdg,powerdgdg,powerddmldg,powerddmdg	! halo-halo and halo-matter power spectra  

 
real(8), dimension(NKbins) :: powerddmldm,powerddmdm ! cross spectra <sij,dm>, <dm^2,dm>, <shift,dm>

real :: time1,time2
integer :: OMP_GET_NUM_THREADS,TID,OMP_GET_THREAD_NUM

integer :: i

radius = 20.0


	
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
call cpu_time(time1)
! Size of FFT Grid	
NCell=512
box=1500.
! Redshift Output
FileNumber=9
! Realization
NodeNumber=1
FileNumber2=999
call genbink(0.003d0,0.5d0,'log')


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
!				> > > FILENAMES < < <
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (FileNumber<10) then	
    write(snapstr,'(a2,i1)') '00',FileNumber
elseif (FileNumber > 9 .and. FileNumber <100) then
    write(snapstr,'(a1,i2)') '0',FileNumber
elseif (FileNumber > 99 .and. FileNumber <1000) then
    write (snapstr, '(i3)') FileNumber
endif

if (FileNumber2<10) then
   write(snapstr2, '(a2,i1)') '00', FileNumber2 
elseif (FileNumber2 > 9 .and. FileNumber2 < 100) then 
   write(snapstr2,'(a1,i2)') '0', FileNumber2
elseif (FileNumber2 >  99 .AND. FileNumber2 <  1000) then 
   write(snapstr2, '(i3)')  FileNumber2
endif

write (*,*) 'snapstr1 = ', snapstr
write(*,*) 'snapstr2 = ', snapstr2

!simulation
!if (NodeNumber<10) then
!    write(nodestr,'(i1)') NodeNumber
!else
!    write(nodestr,'(i2)') NodeNumber
!endif

call getarg(1,nodestr)

if (Ncell<100) then
    write(ncstr,'(i2)') Ncell
elseif (Ncell<1000) then
    write(ncstr,'(i3)') Ncell
elseif (Ncell<10000) then
    write(ncstr,'(i4)') Ncell
endif

doCorrect=1
doBinnedH=1

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! Output File Names
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Extension='NODE'//trim(nodestr)//'_'//assign//'C1_'//trim(ncstr)//'Smoothing_R20_'
Folder='/fast/space/projects/dp002/dc-abid1/projects/powerspec/Data/cross_spectra/quardatic/delta2_delta/'		

PsFileBase='deltanlsquare_deltanl_'
PsOutFile=trim(Folder)//trim(PsFileBase)//trim(Extension)//trim(snapstr2)//'_'//trim(snapstr)//'_log.dat'
write(*,*) 'Writing delta^2-deltah cross spectra to ',PsOutFile



HaloFileBase='deltanlsquare_deltahalo_'
HaloOutFile=trim(Folder)//trim(HaloFileBase)//trim(Extension)//trim(snapstr)//'.dat'
write(*,*) 'Writing Halo cross spectrum to ',HaloOutFile

!Halo Number Mass Bins
NbinB=(/20,60,180,540,1620,4860/)

datadir='/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/reducedGROUPS/'
HalInFile=trim(datadir)//'GRPS_CMS_wmap7_fid_run'//trim(nodestr)//'_'//trim(snapstr)
	

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
! First FFT - Determines FFT strategy
allocate(deltar(1:NCell+2,1:NCell,1:NCell))
call fft3d(deltar,deltar,'f')	
deallocate(deltar)


!///////////////////////////////////////////////////////////////////////////////
!__________________________________DARK MATTER__________________________________
!///////////////////////////////////////////////////////////////////////////////		

write(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
write(*,'(a)') '          > > > Loading dark matter < < <'
write(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'
		


allocate(deltadm(1:NCell+2,1:NCell,1:NCell))
allocate(deltadmnlsquare(1:NCell+2,1:NCell,1:NCell))
        
deltadm=0.
doCorrect=1   
deltadmnlsquare=0

iDat=1; iPos=0; iVel=0; iID=0; iRed=0


! Fiducial Cosmology Final
sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/DATA/'
sf%SNAPBASE = 'wmap7_fid_run'//trim(nodestr)//'_'
sf%SNAPEXT = snapstr
sf%SNAPNFILES = 24


call read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadm)
call normalize(deltadm)


call fft3d(deltadm,deltadm,'f')


if (doCorrect==1) then
    call ciccorrect(deltadm,1.0d0)
endif

call smdir(deltadm,radius,NCell) ! smoothing of the non-linear field

call fft3d(deltadml,deltadml,'e')
deltadmsquare = deltadml*deltadml

call fft3d(deltadmnlsquare,deltadmnlsquare,'f')

    allocate(deltar(1:NCell+2,1:NCell,1:NCell))
    deltar=0.
    call comprod(deltadmnlsquare,deltadm,deltar)
    call powerspectrum(deltar,kBinCnt,kTrueBinC,powerddmdm)	
    deallocate(deltar)

 !Write Output
 open(20,file=PsOutFile,form='formatted',status='replace')
    do i=1,NkBins
	write(20,'(2ES30.10,1I30,1ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerddmdm(i)
    enddo
 close(20)

deallocate(deltadm)

!write(*,'(a)') '\n\n %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
!write(*,'(a)') '          > > > Loading linear dark matter < < <'
!write(*,'(a)') ' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'
		

!allocate(deltadml(1:NCell+2,1:NCell,1:NCell))
!allocate(deltadmsquare(1:NCell+2,1:NCell,1:NCell))



!iDat=1; iPos=0; iVel=0; iID=0; iRed=0

! Fiducial Cosmology Final - CIC version of the Zeldovich initial conditions. This field has small scale artifacts
!sf%SNAPDATA = '/slow/space/cosmos/lss-nongauss/baldauf/SimSuite/wmap7_fid_run'//trim(nodestr)//'/ICs/'
!sf%SNAPBASE = 'wmap7_fid_run'//trim(nodestr)//'_'
!sf%SNAPEXT = snapstr2
!sf%SNAPNFILES = 64


!call read_snap(sF,iDat,iPos,iVel,iID,iRed,NCell,deltadml)
!call normalize(deltadml)                     ! dml in configuation space

!call fft3d(deltadml,deltadml,'f')  ! dml in Fourier space 

!if (doCorrect==1) then
!    call ciccorrect(deltadml,1.0d0)
!endif

!call smdir(deltadml,radius,NCell) ! smoothing of the linear field

!call fft3d(deltadml,deltadml,'e')
!deltadmsquare = deltadml*deltadml

!call fft3d(deltadmsquare,deltadmsquare,'f')

!deallocate (deltadml)


!    allocate(deltar(1:NCell+2,1:NCell,1:NCell))
!    deltar=0.
!    call comprod(deltadmsquare,deltadm,deltar)
!    call powerspectrum(deltar,kBinCnt,kTrueBinC,powerddmldm)	
!    deallocate(deltar)

 !Write Output
! open(20,file=PsOutFile,form='formatted',status='replace')
!    do i=1,NkBins
!	write(20,'(2ES30.10,1I30,1ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerddmldm(i)
!    enddo
! close(20)


!deallocate(deltadm)

!///////////////////////////////////////////////////////////////////////////////
!__________________________________Mass dependent power_________________________
!///////////////////////////////////////////////////////////////////////////////		

        doCorrect=1  
	if (doBinnedH==1) then
	
! the first loop is to count the number of bins which we are going to use to allocate Halo Mass, Pos, and vel of each Halo in each bins	--- cnt is coun ting number of halo bins
	do iMBin=1,5
		write(*,'(a)') '\n\n ***********************************************'
		write(*,'(a)') '          > > > Halos Mass-Binned < < <'
		write(*,'(a)') ' ***********************************************\n\n'
		
		open(11,file=trim(HalInFile),status='old',form='unformatted')
		read(11) NHalTot
		
		print*,'NHalTot',NHalTot
		cnt=0
		do i=1,NHalTot
			read(11) HaloFINAL
			if (HaloFINAL%HN>=NBinB(iMBin) .and. HaloFINAL%HN<NBinB(iMBin+1)) then
					cnt=cnt+1
			endif
		end do
		close(11)
		NHalBin=cnt
		print*,'NHalBin',NHalBin

		allocate(HalM(NHalBin))
		allocate(HalPos(3,NHalBin))
		allocate(HalVel(3,NHalBin))
		
! this second loop is to assign mass, velocity and position to each halo in each bin using 'cnt'. reading from GADGET DATA file-- Question: where did this data come from?		
		
		open(11,file=trim(HalInFile),status='old',form='unformatted')
		read(11) NHalTot
		cnt=0
		do i=1,NHalTot
			read(11) HaloFINAL
			if (HaloFINAL%HN>=NBinB(iMBin) .and. HaloFINAL%HN<NBinB(iMBin+1)) then
			    cnt=cnt+1
			    HalM(cnt)=HaloFINAL%HMass
			    HalPos(:,cnt)=HaloFINAL%HPos(:)
			    HalVel(:,cnt)=HaloFINAL%HVel(:)/sqrt(0.0d0+1.0d0)
			endif
		end do
		close(11)
		
		print*,'Haloes Loaded'
		
! halos has been looad, and now allocating halo overdensity
			allocate(deltag(1:NCell+2,1:NCell,1:NCell))
			deltag=0.d0

			call cicmass(NHalBin,HalPos,deltag)
			MBinC(iMBin)=sum(HalM)/real(NHalBin)

			call normalize(deltag)
			

			call fft3d(deltag,deltag,'f')
		
			if (doCorrect==1) then
 				call ciccorrect(deltag,1.0d0)
			endif
		        
                        call smdir(deltag,radius,NCell) ! smoothing of the non-linear halo field

			allocate(deltar(1:NCell+2,1:NCell,1:NCell))
			deltar=0.d0
			call comprod(deltadmnlsquare,deltag,deltar)
                        call powerspectrum(deltar,kBinCnt,kTrueBinC, powerddmdg(:,iMBin))	
			deallocate(deltar)
							
			deallocate(deltag)

			
			deallocate(HalM)
			deallocate(HalVel)
			deallocate(HalPos)
	
		enddo
	endif

	if (doBinnedH==1) then


	
	open(23,file=trim(HaloOutFile),form='formatted',status='replace')
 	do i=1,NkBins
		write(23,'(2ES30.10,1I30,11ES30.10)') kbinc(i),ktruebinc(i),kbincnt(i),powerddmdm(i),powerddmdg(i,:)
   	enddo
   	close(23)


	
 	endif	
				

deallocate(deltadmnlsquare)

call cpu_time(time2)
time2=(time2-time1)
write(*,'(a,f8.2)') 'All done in', time2


end program power

